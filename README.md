# albina-blogcache

## Requirements

- NodeJS
- ForeverJS: install with `npm install -g forever`


## Development

Use `npm install` to download the necessary packages.

Use `forever start ./blog.js` to run the server locally.

You can use `forever list`, `forever restart` and `forever stop` to manage
your running daemon process.

Use `npm run deploy` to deploy your local changes to the production server.
